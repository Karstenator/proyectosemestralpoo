package Restoran.Modelo;
import Restoran.Modelo.Cliente;
import java.time.LocalDate;
import java.util.ArrayList;
public class Pedido {
    public enum Estado{
        PENDIENTE,
        ENTREGADO
    }
    private int numero;
    private LocalDate fecha;
    private Estado estado;
    private Cliente cliente;
    private Repartidor repartidor;
    private ArrayList<LineaPedido> lineasPedido;
    public Pedido(int numero, LocalDate fecha, Cliente cliente) {
        this.numero = numero;
        this.fecha = fecha;
        this.cliente = cliente;
        cliente.addPedido(this);
        estado = Estado.PENDIENTE;
        repartidor = null;
        lineasPedido = new ArrayList<>();
    }
    public int getNumero(){

        return numero;
    }
    public LocalDate getFecha(){

        return fecha;
    }
    public Estado getEstado(){

        return estado;
    }
    public Cliente getCliente(){

        return cliente;
    }
    public Repartidor getRepartidor(){

        return repartidor;
    }
    public void addProducto(Producto producto, int cantidad){
        lineasPedido.add(new LineaPedido(this,producto,cantidad));
    }
    public int getMontoTotal() {
        int monto=0;
        for (LineaPedido lineaPedido: lineasPedido) {
            monto += lineaPedido.getSubTotal();
        }
        return monto;
    }
    public void setRepartidor(Repartidor repartidor){
        this.repartidor = repartidor;
        repartidor.addPedido(this);
    }
    public void setEntregado(){

        estado = Estado.ENTREGADO;
    }
}